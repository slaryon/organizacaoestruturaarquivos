import time
import os
import math

CEP_buscado = str(input('Digite o cep a ser buscado: '))
print CEP_buscado
#CEP_buscado = "20561230"
inicio = time.clock()
f = open('cep_ordenado.dat','r')

#Divide o tamanho do arquivo pelo tamanho da struct e me da o numero de linhas
registros = (os.path.getsize('cep_ordenado.dat') / 300)

#index do arquivo
b_ini = 0
b_meio = int(math.floor(registros/2))
b_fim = registros

#ler cep
def ler_cep(index):
    f.seek(290 + (index * 300),0)
    cep = f.read(8)
    return cep

cep_meio = ler_cep(b_meio)
controle = True
contador = -1
while(controle):
    contador +=1    
    if (cep_meio > CEP_buscado):
        b_fim = b_meio
	b_meio = int(math.floor((b_ini + b_fim)/2)-1)
        #b_meio = int(math.floor((b_ini + b_fim)/2))
        cep_meio = ler_cep(b_meio)
        continue
    elif(cep_meio < CEP_buscado):
        b_ini = b_meio
	b_meio = int(math.ceil((b_fim + b_meio)/2+1))
        # b_meio = int(math.ceil((b_fim + b_meio)/2))
        cep_meio = ler_cep(b_meio)
        continue        
    if cep_meio == CEP_buscado:
        controle = False
            # Endereco
        f.seek(b_meio * 300,0)
        endereco = f.read(72)
        print 'Endereco: ' + endereco.strip()
            # Bairro
        bairro = f.read(72)
        print 'Bairro: ' + bairro.strip()
            # Cidade
        cidade = f.read(72)
        print 'Cidade: ' + cidade.strip()
            # Estado
        estado = f.read(72)
        print 'Estado: ' + estado.strip()
            # Sigla
        sigla = f.read(2)
        print 'sigla: ' + sigla
            # CEP print cep_meio
        print 'CEP: ' + cep_meio
	print 'Iteracoes: ' + str(contador)
        break
    if (b_ini > b_fim):
        print 'CEP nao encontrado, verifique o CEP'
        break

f.close()
print 'Levou: ' + str(time.clock() - inicio)



