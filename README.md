# README #
Trabalho da Disciplina de Estrutura de Arquivos 2017.2 (CEFET-RJ)

### Objetivo do reposit�rio? ###

	Armazenar os c�digos feitos durante a disciplina e o projeto final 

### Projeto Final - noWorkflow ###

	1. Tutorial de como utilizar o noWorkflow
	2. problemas encontrados
	3. Explicar gr�ficos (fatorial)
	4. Mostrar gr�fico de um programa maior (forca)
	5. Benchmark de programas com muito processamento (fatorial 60, 100, 10000)
	6. Benchmark de programas com mais acesso a disco (aplica��o a escolher)
	
	Projeto sendo feito em jupyter (utilizando markdown) + noWorkflow (precisa de dependencias)

### Aluno: ###
*Bruno Cavalcante Rego*
