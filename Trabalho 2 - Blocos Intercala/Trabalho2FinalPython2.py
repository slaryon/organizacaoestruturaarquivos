import os
import time

inicio = time.clock()

#Bloco de constantes
TamRegistro = 300
nBlocos = int(209792100 / (10000*300))
numeroLinhas = 10000
arquivoBase = 'cep.dat' # Tamanho 209792100 / 300 = 699307 
# Tamanho = 300 * numBlocos * numeroLinhas
# 209792100 = 300 * numBlocos * 10000
arquivoTrabalho = 'CEPSample.dat'
tamanhoArquivo = os.path.getsize(arquivoBase) 

def criar_Arquivo_Teste(NumeroLinhas):
    '''Cria um arquivo de teste com NumeroLinhas linhas
    demais arquivos devem ser indexados no topo desse
    (eu n sei porque escrevi isso, apagar se nao lembrar - capaz de ser sono)
    '''
    w = open(arquivoTrabalho,'w')
    f = open(arquivoBase,'r')
    # Deve atribuir para uma variavel para poder escrever
    b = f.read(TamRegistro*NumeroLinhas)
    w.write(b)
    f.close()
    w.close()

def criar_blocos():
    '''
    Cria os blocos com base no numero e tamanho do struct configurados acima
    nao precisa fechar com with
    retorna uma lista com os arquivos criados para posterior acesso
    '''
    with open(arquivoBase,'r') as f:
        j = 0
        leia = TamRegistro * numeroLinhas
        listaArquivo =[]
        while not(j == nBlocos+1):
            filename = "bloco" + str(j) + ".dat"            
            listaArquivo.append(filename)
            w = open(filename,'w')
            b = f.read(leia)
            w.write(b)
            w.close()            
            j +=1
        return listaArquivo

def ordenar_bloco(lista_arquivos):    
    for arquivo in lista_arquivos:
        listaOrdenada = []
        with open(arquivo,'r+') as f:
            for b in f:
                bCEP = b[290:298]
                c = (bCEP,b)
                listaOrdenada.append(c)
            # sort e escreve
                # Usar assim ou com 'r' e 'w' ?    
            listaOrdenada = sorted(listaOrdenada)
            f.seek(0,0)
            for registro in listaOrdenada:
                f.write(registro[1])
##MAIN##

# Cria um arquivo para testar com numeroLinhas linhas
#criar_Arquivo_Teste(numeroLinhas)
#retorna uma lista dos arquivos criados para posterior acesso
lista_arquivos = criar_blocos()
#print("Foram criados", len(lista_arquivos), "arquivos com sucesso.")
#print("Seus nomes sao:",str(lista_arquivos))
ordenar_bloco(lista_arquivos)
fim=time.clock()
print str(fim-inicio)
