import os

#Bloco de constantes
TamRegistro = 300
nBlocos = 5
numeroLinhas = 22
arquivoBase = 'cep.dat'
arquivoTrabalho = 'CEPSample20.dat'
tamanhoArquivo = os.path.getsize(arquivoTrabalho)

def criar_Arquivo_Teste(NumeroLinhas):
    '''Cria um arquivo de teste com NumeroLinhas linhas
    demais arquivos devem ser indexados no topo desse
    (eu n sei porque escrevi isso, apagar se não lembrar - capaz de ser sono)
    '''
    w = open(arquivoTrabalho,'w')
    f = open(arquivoBase,'r')
    # Deve atribuir para uma variável para poder escrever
    b = f.read(TamRegistro*NumeroLinhas)
    w.write(b)
    f.close()
    w.close()

def criar_blocos():
    '''
    Cria os blocos com base no número e tamanho do struct configurados acima
    não precisa fechar com with
    retorna uma lista com os arquivos criados para posterior acesso
    '''
    with open(arquivoTrabalho,'r') as f:
        j = 0
        leia = nBlocos * TamRegistro
        listaArquivo =[]
        while not(j == nBlocos):
            filename = "bloco" + str(j) + ".dat"            
            listaArquivo.append(filename)
            w = open(filename,'w')
            b = f.read(leia)
            w.write(b)
            w.close()            
            j +=1
        return listaArquivo

def ordenar_bloco(lista_arquivos):    
    for arquivo in lista_arquivos:
        listaOrdenada = []
        with open(arquivo,'r+') as f: # https://www.tutorialspoint.com/python/python_files_io.htm
            for b in f:
                bCEP = b[290:298]
                c = (bCEP,b)
                listaOrdenada.append(c)
            # sort e escreve
                # Usar assim ou com 'r' e 'w' ?    
            listaOrdenada = sorted(listaOrdenada)
            f.seek(0,0)
            for registro in listaOrdenada:
                f.write(registro[1])
##MAIN##

# Cria um arquivo para testar com numeroLinhas linhas
criar_Arquivo_Teste(numeroLinhas)
#retorna uma lista dos arquivos criados para posterior acesso
lista_arquivos = criar_blocos()
print("Foram criados", len(lista_arquivos), "arquivos com sucesso.")
print("Seus nomes são:",str(lista_arquivos))

ordenar_bloco(lista_arquivos)
