import os

#Bloco de constantes
TamRegistro = 300
nBlocos = 5
numeroLinhas = 22
arquivoBase = 'cep.dat'
arquivoTrabalho = 'CEPSample20.dat'
tamanhoArquivo = os.path.getsize(arquivoTrabalho)
#nRegistros = nao precisa no momento


#ler cep a partir do arquivo
def ler_cep(index):
    f.seek(290 + (index * 300),0)
    cep = f.read(8)
    return cep

def criar_Arquivo_Teste(NumeroLinhas):
    '''Cria um arquivo de teste com NumeroLinhas linhas
    demais arquivos devem ser indexados no topo desse
    (eu n sei porque escrevi isso, apagar se não lembrar - capaz de ser sono)
    '''
    #pode melhorar passando seek
    w = open(arquivoTrabalho,'w')
    f = open(arquivoBase,'r')
    #deve atribuir para uma variável para poder escrever
    b = f.read(TamRegistro*NumeroLinhas)
    w.write(b)
    f.close()
    w.close()

# passar nRegistros, filepointer
def criar_blocos():
    '''
    Cria os blocos com base no número e tamanho do struct configurados acima
    não precisa fechar com with
    retorna uma lista com os arquivos criados para posterior acesso
    '''
    with open(arquivoTrabalho,'r') as f:
        j = 0
        leia = nBlocos * TamRegistro
        listaArquivo =[]
        while not(j == nBlocos):
            filename = "bloco" + str(j) + ".dat"            
            listaArquivo.append(filename)
            w = open(filename,'w')
            b = f.read(leia)
            w.write(b)
            w.close()            
            j +=1
        return listaArquivo
#        if (not(tamanhoArquivo % leia) == 0):
#            filename = "bloco" + str(j) + ".dat"
#            w = open(filename,'w')
#            b = f.read(leia)
#            w.write(b)
#            w.close()

# http://pythoncentral.io/how-to-sort-python-dictionaries-by-key-or-value/
# https://wiki.python.org/moin/TimeComplexity
# https://www.dropbox.com/sh/xeqvap4abwpnd43/AABMy6UxVbspwXu2GHDfuw1wa
def ordenar_bloco(lista_arquivos):    
#    i=0
    for arquivo in lista_arquivos:
        listaOrdenada = []
        with open(arquivo,'r+') as f: # https://www.tutorialspoint.com/python/python_files_io.htm
            for b in f:
    #            b = f.read(TamRegistro)
                bCEP = b[290:298]
                c = (bCEP,b)
                listaOrdenada.append(c)
            # sort e escreve
            # Usar assim ou com 'r' e 'w'    
            listaOrdenada = sorted(listaOrdenada)
            f.seek(0,0)
            for registro in listaOrdenada:
                f.write(registro[1])
                
    #            i += 1
#        print(i)
        #print(listaOrdenada)
#        listaOrdenada = sorted(listaOrdenada)
        #print(listaOrdenada)
#        print(arquivo)
        #f = open(arquivo,'w')
        #with open(arquivo,'w') as f:
 #       for registro in listaOrdenada:
 #           f.write(registro[1])

#        print(f.closed)
def intercala(lista_arquivos):
    

# CANT USE DICT because there are some CEPs that are dupicated at least =/ LOOP OVER FILE
# code is working (ordering ok, but snce dicts dont accept double keys
# posso checar se existe e clocar um 0 antes para não sobreescrver... gastaria muito processamento
# é muita gambiarra....
#        for b in f:
        #b = f.read(TamRegistro)        
#            bCEP = b[290:298]            
#
#            dict[bCEP] = [b];
#    print(sorted(dict)) #return an ordered array    
#    print(dict)

##MAIN##

# Cria um arquivo para testar com x linhas
criar_Arquivo_Teste(numeroLinhas)

#retorna uma lista dos arquivos criados para posterior acesso
lista_arquivos = criar_blocos()
print("Foram criados", len(lista_arquivos), "arquivos com sucesso.")
print("Seus nomes são:",str(lista_arquivos))


# gerar arquivo auxiliar em disco para gerar um 'indice' para o método de ordenação e atualizar a cada passada
# Operar com alguma estrutura pra dar apoio ao sort, como CEP, (inicio, fim), arquivo?, e dar update sempre q alterar

ordenar_bloco(lista_arquivos)

intercala(lista_arquivos)

#with open('bloco0.dat','r') as f:
    #teste = sorted(f)
    #print(teste)

# sorted() sorted(iterable, *, key=None, reverse=False)¶
# where key may be a function to compare elements
# https://docs.python.org/3/howto/sorting.html#sortinghowto

'''
def cmp_to_key(mycmp):
    'Convert a cmp= function into a key= function'
    class K:
        def __init__(self, obj, *args):
            self.obj = obj
        def __lt__(self, other):
            return mycmp(self.obj, other.obj) < 0
        def __gt__(self, other):
            return mycmp(self.obj, other.obj) > 0
        def __eq__(self, other):
            return mycmp(self.obj, other.obj) == 0
        def __le__(self, other):
            return mycmp(self.obj, other.obj) <= 0
        def __ge__(self, other):
            return mycmp(self.obj, other.obj) >= 0
        def __ne__(self, other):
            return mycmp(self.obj, other.obj) != 0
    return K
'''


