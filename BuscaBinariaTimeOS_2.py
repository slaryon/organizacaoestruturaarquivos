import time
import os
import math

# Pede para o usuario entrar com o cep nao checa se tem 8 digitos
CEP_buscado = str(input('Digite o cep a ser buscado: '))
# inicia contagem de tempo
inicio = time.clock()
f = open('cep_ordenado.dat','r')

#Divide o tamanho do arquivo pelo tamanho da struct e me da o numero de linhas
registros = (os.path.getsize('cep_ordenado.dat') / 300)

#index do arquivo
b_ini = 0
b_meio = int(math.floor(registros/2))
b_fim = registros

#ler cep a partir do arquivo
def ler_cep(index):
    f.seek(290 + (index * 300),0)
    cep = f.read(8)
    return cep

cep_meio = ler_cep(b_meio)
controle = True
contador = -1
while(controle):
    contador +=1    
    if (cep_meio > CEP_buscado):
        b_fim = b_meio
	b_meio = int(math.floor((b_ini + b_fim)/2)-1)
        #b_meio = int(math.floor((b_ini + b_fim)/2))
        cep_meio = ler_cep(b_meio)
        continue # acho que poderia remover 
    elif(cep_meio < CEP_buscado):
        b_ini = b_meio
	b_meio = int(math.ceil((b_fim + b_meio)/2+1))
        # b_meio = int(math.ceil((b_fim + b_meio)/2))
        cep_meio = ler_cep(b_meio)
        continue        
    if cep_meio == CEP_buscado:
	# Se verdadeiro imprime os dados
        controle = False # poderia remover, mas fica didatico ja que usei no while
            # Endereco - tamanho 72
        f.seek(b_meio * 300,0)
        endereco = f.read(72)
        print 'Endereco: ' + endereco.strip()
            # Bairro - tamanho 72 + remove espacos
        bairro = f.read(72)
        print 'Bairro: ' + bairro.strip()
            # Cidade - tamanho 72 + remove espacos
        cidade = f.read(72)
        print 'Cidade: ' + cidade.strip()
            # Estado - tamanho 72 + remove espacos
        estado = f.read(72)
        print 'Estado: ' + estado.strip()
            # Sigla - tamanho 2
        sigla = f.read(2)
        print 'sigla: ' + sigla
            # CEP print cep_meio
        print 'CEP: ' + cep_meio
            # numero de iteracoes que o programa levou
	print 'Iteracoes: ' + str(contador)
        break
    if (b_ini > b_fim):
        print 'CEP nao encontrado, verifique o CEP'
        break

f.close()
print 'Levou: ' + str(time.clock() - inicio)



